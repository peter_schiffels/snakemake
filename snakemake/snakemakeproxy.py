import sys
from snakemake.io import Namedlist
args = sys.argv[1:]


def grouper(s):
    """
    Groups args into (argname, value) tuples.
    """
    last = None
    for i in s:
        if '=' in i:
            yield tuple(i.split('='))
            continue
        if last:
            yield last, i
            last = None
            continue
        last = i

grouped = list(grouper(args))

result = Namedlist()
for arg, val in grouped:
    if not arg.startswith('--'):
        raise ValueError(
            'Argument "{0}" does not start with "--". Args provided: {1}'
            .format(arg, grouped)
        )
    split = arg.lstrip('-').split('.')
    current = result
    n = len(split)
    for i, name in enumerate(split):
        if i < (n - 1):
            current.append(Namedlist())
            current.add_name(name)
            current = current[-1]
        else:
            current.append(val)
            current.add_name(name)

snakemake = result
